---
title: "Resume"
date: 2021-03-18T03:46:25Z
slug: "Resume"
description: "a tiny overview of Andre Roberge"
keywords: []
draft: false
tags: [resume]
math: false
toc: true
---

# Andre Roberge
Seattle, WA <br>
[Email](mailto:andre.p.roberge@outlook.com) <br>
[LinkedIN](https://b.link/Andre-Roberge-LinkedIn) <br>
[Credly-Badges](https://b.link/Credly-Badges) <br>

## Summary

My current position within the mission defense team builds upon all of the
training and experience I’ve gained over my career. In this cybersecurity role I
have become an expert in many types of network traffic. My tenacity to learn new
things at a deeper level and tackle new challenges is what separates me from my
peers. Lastly, my passion for understanding technology extends beyond my current
role and is highlighted by my community involvement:

Art of Network Engineering Staff Member – Lead study groups, provide resources to
community members and constant contributor on the [blog](https://artofnetworkengineering.com/author/roberge2/).

Cisco Champion 2020-21 – Selected as a Cisco ambassador based on my strong social
presence discussing Cisco products, educational pursuits, and certifications.

## Certifications

GIAC Certified Intrusion Analyst, CCNA, JNCIA, CompTIA Security+ and A+

## Experience

Network Analyst, Mission Defense Team, Washington ANG, JBLM <br>
(September 2020 – Present) <br>
Key Duties: Networking and Infrastructure Subject Matter Expert <br>
• Operate a distributed virtual stack consisting of Zeek, Suricata, Elasticsearch, and Arkime <br>
• Lead trainer on primary defensive cyber operations functions specializing in network artifacts <br>
• Installed, configured and trained team on our backend cyber operations infrastructure kit consisting of Dell and Cisco Switches, Garland TAPs, and multiple Dell R440 servers <br>
<br>
Languages/Data formats: bash, PowerShell, JSON, Python, YAML <br>
Operating systems: RHEL, VMware ESXi, Windows 10, Cisco IOS-XE, Dell OS 10 <br>
Frameworks: kubernetes (user), ansible (user)

Infrastructure Technician, 225th Support Squadron, Washington ANG, JBLM <br>
(January 2020 - September 2020) <br>
Key Duties: Equipment warranty and lifecycle management, project implementation, execution, training, and mentoring team members within the work center <br>
• Maintained Cisco and Juniper network equipment from core and access switches to internet facing routers supporting approximately 1000 client devices <br>

Client Systems Technician, 225th Support Squadron, Washington ANG, JBLM <br>
(July 2018- January 2020)<br>
Key Duties: Offer one-on-one personal support for all classified and unclassified systems, personal wireless devices and printers utilizing ticketing system and tracking man hour requirements per task <br>
• Managed work center annual equipment inventory ensuring proper lifecycle, usability, and accountability of equipment <br>
• Led multi-work center training that imparted guidance and study methodologies for CCNA exam <br>

## Education

Bachelor of Arts in Philosophy with Honors
University of Washington, Seattle

‘One size never fits all’ - RFC 1925 [rule 10]
